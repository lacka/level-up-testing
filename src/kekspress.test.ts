import { Kekspress, Keksception } from './kekspress';

describe('Kekspress', () => {
  describe('getPassengers', () => {
    test('should return 0 passengers if the Kekspress is empty', () => {
      const kekspress = new Kekspress(9);
      expect(kekspress.getPassengers()).toHaveLength(0);
    });

    test('should return 1 passenger if the Kekspress 1 board called', () => {
      const kekspress = new Kekspress(9);
      kekspress.board('Kekito', 1);
      expect(kekspress.getPassengers()).toHaveLength(1);
    });
  });

  describe('nextStop', () => {
    test('should return 0 passenger if the Kekspress 1 board called and on the next stop', () => {
      const kekspress = new Kekspress(9);
      kekspress.board('Kekito', 1);
      kekspress.nextStop();
      expect(kekspress.getPassengers()).toHaveLength(0);
    });

    test('should return 1 passenger if the Kekspress 1 board called with 2 stop and on the next stop', () => {
      const kekspress = new Kekspress(9);
      kekspress.board('Kekito', 2);
      kekspress.nextStop();
      expect(kekspress.getPassengers()).toHaveLength(1);
    });

    test('should return 0 passenger if the Kekspress 1 board called with 2 stop and go 2 stop', () => {
      const kekspress = new Kekspress(9);
      kekspress.board('Kekito', 2);
      kekspress.nextStop();
      kekspress.nextStop();
      expect(kekspress.getPassengers()).toHaveLength(0);
    });
  });

  describe('board', () => {
    test('should return 4 passenger if the Kekspress 4 board called', () => {
      const kekspress = new Kekspress(9);
      kekspress.board('Kekito', 2);
      kekspress.board('Kekker', 2);
      kekspress.board('Keke', 2);
      kekspress.board('Kekkerino', 2);
      expect(kekspress.getPassengers()).toHaveLength(4);
    });

    test('should error if the Kekspress board with the same name', () => {
      const name = 'Kekker';
      const kekspress = new Kekspress(9);
      kekspress.board(name, 2);
      expect(() => {
        kekspress.board(name, 2);
      }).toThrow(new Keksception(`Name ${name} already boarded`));
    });

    test('should return 0 passenger if the Kekspress 1 board called but max seat is 0', () => {
      const name = 'Kekker';
      const kekspress = new Kekspress(0);
      kekspress.board(name, 2);
      expect(kekspress.getPassengers()).toHaveLength(0);
    });
  });

  describe('getOff', () => {
    test('should return 0 passenger if the Kekspress 1 board and 1 getOff called', () => {
      const name = 'Kekker';
      const kekspress = new Kekspress(9);
      kekspress.board(name, 2);
      kekspress.getOff(name);
      expect(kekspress.getPassengers()).toHaveLength(0);
    });

    test('should return 1 passenger if the Kekspress 1 board and 1 getOff with different name called', () => {
      const name = 'Kekker';
      const kekspress = new Kekspress(9);
      kekspress.board(name, 2);
      kekspress.getOff('Kekito');
      expect(kekspress.getPassengers()).toHaveLength(1);
    });
  });
});
